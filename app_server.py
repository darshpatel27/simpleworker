from simpleworker.server import Server
from simpleworker.queue import Queue

host = '127.0.0.1'
port = 8010
redis_host = '127.0.0.1'
redis_port = 6379

app_server = Server(host,port,redis_host,redis_port)
queue = Queue(redis_host,redis_port)
while 1:
	job_data = app_server.get_job_data()
	queue_name = app_server.get_queue_name()
	if job_data and queue_name:
		job_id = queue.add_job(queue_name, job_data)
		print 'Job queued succesfully'
		print 'queue_name = ' +queue_name
		print 'job_id = ' + job_id
		print 'job_data = ' + job_data +'\n'
		app_server.send_server_response(job_id)