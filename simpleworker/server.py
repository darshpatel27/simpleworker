from simplesocket import SimpleSocket
from queue import Queue
import json

class Server:
	"""
	Worker server that handles queuing logic
	"""
	
	MAX_PAYLOAD_SIZE = 1024

	def __init__(self,host,port,redis_host,redis_port):
		self.host = host
		self.port = port
		self.server_socket = self.get_server() 
		self.redis_host = redis_host
		self.redis_port = redis_port

	def get_server(self):
		"""
		Get the socket connection for worker server
		"""
		server_socket = SimpleSocket(self.host,self.port)
		server_socket.get_server_socket()
		#server_socket.get_server_socket_conn()
		return server_socket

	def get_client_data(self):
		"""
		Get the raw data received from worker client
		"""
		return self.server_socket.conn.recv(Server.MAX_PAYLOAD_SIZE)

	def get_job_data(self):
		"""
		Get the job data that needs to be queued
		"""
		self.server_socket.get_server_socket_conn()
		client_data = self.get_client_data()
		if client_data:
			try:
				client_data = json.loads(client_data)
			except ValueError, e:
				return
			self.queue_name = client_data['queue_name']
			client_data.pop('queue_name', None)
			job_data = json.dumps(client_data)
			return job_data

	def get_queue_name(self):
		"""
		Get the target queue for the current job
		"""
		return self.queue_name

	def send_server_response(self,job_id):
		"""
		Send the unique job_id to client for tracking
		"""
		self.server_socket.send_server_data(job_id)
		self.server_socket.conn.close()




