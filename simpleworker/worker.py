from queue import Queue
import redis

class Worker:
	"""
	Worker to process the job data queued by server
	"""

	def __init__(self,redis_host,redis_port):
		self.queue = Queue(redis_host,redis_port)
		self.queue_name = 'default'

	def process(self):
		"""
		Returns the next job in the queue
		"""
		return self.queue.get_job(self.queue_name)

	def get_queue(self,queue_name):
		"""
		Select the queue that needs to be processed by worker
		"""
		self.queue_name = queue_name

	def save_result(self, job_id, result):
		"""
		Save the job output corresponding to the job_id
		"""
		self.queue.update_job(job_id, result)