import socket
import sys

class SimpleSocket:
	"""
	Simple socket wrapper for managing client and server socket connections
	"""
	MAX_CONN = 10
	MAX_PAYLOAD_SIZE = 1024

	def __init__(self,host,port):
		self.host = host
		self.port = port

	def get_client_socket(self):
		"""
		Get socket connection for worker client
		"""
		try:
		    #create an AF_INET, STREAM socket (TCP)
			client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		except socket.error, msg:
		    raise Exception('Failed to create socket. Error code: ' + \
		    	str(msg[0]) + ' , Error message : ' + msg[1])
		try:
			client.connect((self.host, self.port))
		except Exception, e:
			sys.exit('Failed to connect. Make sure worker server is up & running')
		self.client = client

	def get_server_socket(self):
		"""
		Get socket connection for worker server
		"""
		try:
		    #create an AF_INET, STREAM socket (TCP)
			server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		except socket.error, msg:
		    raise Exception('Failed to create socket. Error code: ' + \
		    	str(msg[0]) + ' , Error message : ' + msg[1])
		
		server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

		try:
		    server.bind((self.host, self.port))
		except socket.error , msg:
		    raise Exception('Bind failed. Error Code : ' + \
		    	str(msg[0]) + ' Message ' + msg[1])
	 
		server.listen(SimpleSocket.MAX_CONN)
		self.server = server

	def get_server_socket_conn(self):
		"""
		Accept data from client
		"""
		conn, addr = self.server.accept()
		self.conn = conn

	def send_client_data(self,data):
		"""
		Send data to client socket from server socket
		"""
		try:
			self.client.sendall(data)
			job_id = self.client.recv(SimpleSocket.MAX_PAYLOAD_SIZE)
			#self.client.close()
		except socket.error, msg:
			raise Exception('Fail to send msg' + str(msg[0]) + \
				' , Error message : ' + msg[1])
		return job_id

	def send_server_data(self,data):
		"""
		Send data to server socket from client socket
		"""
		try:
			self.conn.sendall(data)
		except socket.error, msg:
			raise Exception('Fail to send msg' + str(msg[0]) + \
				' , Error message : ' + msg[1])
		return data



