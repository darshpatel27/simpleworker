import redis
import uuid
import json

class Queue:
	"""
	Worker queue for job processing, implemented using redis
	"""

	JOB_MAP='job_map'

	def __init__(self,redis_host,redis_port):
		self.queue = redis.StrictRedis(redis_host,redis_port,0)
	
	def add_job(self,queue_name,job_data):
		"""
		Adds a new job to the worker queue
		"""
		job_id = str(uuid.uuid4())
		if job_id:
			job_data = self.insert_job_id(job_data,job_id)
			if job_data:
				self.save_job(job_id)
				self.queue.rpush(queue_name,job_data)
				return job_id
		return None

	def get_job(self,queue_name):
		"""
		Returns a job from job queue
		"""
		return self.queue.lpop(queue_name)

	def save_job(self,job_id):
		"""
		Save the job_id of current job
		"""
		self.queue.hset(Queue.JOB_MAP,job_id,None)

	def insert_job_id(self,job_data,job_id):
		"""
		Add job_id to job_data for tracking results
		"""
		data = json.loads(job_data)
		data['job_id'] = job_id
		return json.dumps(data)

	def update_job(self,job_id, result):
		"""
		Once the job is processed, map the result to corresponding job_id
		"""
		self.queue.hset(Queue.JOB_MAP,job_id, result)



