from simplesocket import SimpleSocket
import json
import sys

class Client:
	"""
	Worker client for sending data to server
	"""
	def __init__(self,host,port):
		self.host = host
		self.port = port
		self.queue_name = 'default'

	def put(self,data):
		"""
		Transfers job data from client to server using socket	
		"""
		self.client_socket = self.get_client()
		job_data = self.get_job_data(data)
		if job_data:
			job_id = self.client_socket.send_client_data(job_data)
			if job_id:
				self.client_socket.client.close()
				return job_id
		return None

	def get_client(self):
		"""
		Get a new socket connection for client
		"""
		client_socket = SimpleSocket(self.host,self.port)
		client_socket.get_client_socket()
		return client_socket

	def set_queue(self,name):
		"""
		Queue name where this client data should be queued
		"""
		self.queue_name = name

	def get_job_data(self,data):
		"""
		Get job data to be transferred from client to server
		"""
		if self.is_valid_data(data):
			job_data = json.loads(data)
			job_data['queue_name'] = self.queue_name
			job_data = json.dumps(job_data)
			return job_data

	def is_valid_data(self, data):
		"""
		Check whether the job data is in valid json format
		"""

		try:
			json_object = json.loads(data)
		except ValueError, e:
			sys.exit('Invalid job data, please provide valid json data')
		return True



