from simpleworker.worker import Worker
import json

redis_host = '127.0.0.1'
redis_port = 6379
queue_name = 'worker_queue'

def process_job_data(job_data,job_id):
	result = job_data['a']+job_data['b']
	worker.save_result(job_id,result)
	return result


worker = Worker(redis_host,redis_port)
worker.get_queue(queue_name)

while 1:
	job_data = worker.process()
	if job_data:
		print 'Processing job'

		job_data = json.loads(job_data)
		job_id = job_data.pop('job_id', None)
		result = process_job_data(job_data,job_id)

		print 'Job id = ' + job_id
		print 'Job data = ' + str(job_data)
		print 'Job output = ' + str(result) +'\n'