How to test the library?

1. Checkout the GIT repo & go to the top level directory
2. You will find three test scripts called 
	app_client.py
	app_server.py
	app_worker.py

3. Now open three terminal windows/tabs
4. Run the scripts in following order
	python app_server.py
	python app_worker.py
	python app_client.py



