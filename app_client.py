from simpleworker.client import Client
from random import randint
import time
import json

host = '127.0.0.1'
port = 8010

worker = Client(host,port)
worker.set_queue('worker_queue')

while 1:
	data = {}
	data['a'] = randint(0,99)
	data['b'] = randint(0,99)
	data = json.dumps(data)
	job_id = worker.put(data)

	print 'Job created successfully' 
	print 'job_id = '+job_id
	print 'job_data = '+data + '\n'

	time.sleep(3)